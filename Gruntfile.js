module.exports = function(grunt){
	grunt.initConfig({
		uglify: {
			group1: {
				files: {
					'js/scripts.min.js' : ['js/scripts.js']
				}				
			}
		}, // Minify JS
		sass : {
			group1: {
				files: {
					'view.css' : 'view.scss'
				}
			}
		}, // SASS
		cssmin: {
			group1: {
				files: {
					'view.min.css' : ['view.scss']
				}
			}
		},
		watch: {
			css: {
				files: '*.scss',
				tasks: ['sass'],
				options: {
					livereload : true,
				},
			},
		}
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('build', ['uglify','cssmin','sass', 'watch']);
}